package work;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.io.IOException;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import work.logic.*;

public class Controller {

    private Stage stage;
    //    private static String characterName = "";
    public static int characterClass = 1; //todo: rework with direct class input, it's doesn't work!
    private Hero hero = Hero.getInstance();
    private int round = 0; //todo: пока что тут
    private Enemy enemy = Enemy.generate(); //todo: пока что тут

    //    Application init
    void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    TextField w2TextFieldName;
    @FXML
    TextArea w4BattleLog;
    @FXML
    ImageView w2ImageRace, w2ImageClass, MapLocation1, w4ImgBack, w4ImgNext, w4ImageEnemy, w5SkillUp1, w5SkillUp2, w5SkillUp3, w5SkillUp4;
    @FXML
    Label w2LabelRaceDescription, w2LabelClassDescription, w2LabelName, heroHP, enemyHP, w4HeroStat1, w4HeroStat2, w4HeroStat3, w4HeroStat4, w4HeroStat5, w4HeroStat6, w4EnemyStat1, w4EnemyStat2, w4EnemyStat3, w4EnemyStat4, w4EnemyStat5, w4EnemyStat6, w5Name, w5Class, w5Level, w5Experience, w5EnemyCounter, w5Gold, w5Strength, w5Agility, w5Vitality, w5Intellect, w5Attack, w5Armor, w5HP, w5MP, w5Offensive, w5Defensive, w5LearningPoints, w5SkillPoints, w7Gold;
    @FXML
    Button w5TEST, MapLocation2, MapLocation3, w4Enemy1, w4Enemy2, w4Enemy3, w4Enemy4, w4Enemy5, w4Enemy6, w4Enemy7, w4Enemy8, w4Enemy9, w4Enemy10, w4Enemy11, w4Enemy12, inventoryBack, inventoryNext;
    @FXML
    GridPane mapLocationDescriptionGrid, inventoryGrid, shopGrid, w4MobGrid; //skillSelectionGrid
    @FXML
    ProgressBar heroHPbar, enemyHPbar;
    @FXML
    HBox w4Skills;
    @FXML
    VBox w4VBox2, w5ItemDescription;
    @FXML
    Pane w5WeaponBox, w5ChestBox, w5Equipment;
    @FXML
    HBox abilityPanel;

    //    Window1 HelloScreen

    public void w1BtnNew() throws IOException {
        new Switcher(stage, "CreationScreen.fxml", "sandboxFX w2");
    }

    public void w1BtnLoad() throws IOException {
    }

    public void w1BtnExit() throws IOException {
        System.exit(0);
    }

    //    Window2   CreationScreen

    public void w2BtnBack() throws IOException {
        new Switcher(stage, "HelloScreen.fxml", "sandboxFX w1");
    }

    public void w2BtnHuman() throws IOException {
        w2ImageRace.setImage(new Image(getClass().getResource("img/human.jpg").toExternalForm()));
        w2LabelRaceDescription.setText("Humans are among the youngest races in the world, but they make up for it by being the most populous. With life spans generally shorter than the other races, humans strive all the harder to achieve great heights in empire building, exploration, and magical study. Their aggressive and inquisitive nature lead the human nations to become active and influential in the world.");
    }

    public void w2BtnElf() throws IOException {
        w2ImageRace.setImage(new Image(getClass().getResource("img/elf.jpg").toExternalForm()));
        w2LabelRaceDescription.setText("Elves are one of the main races living in the Kingdoms. After the dragons and before the arrival of people, the elves ruled a huge territory. But under the onslaught of people, they began to retreat into quiet forests and now their number is only a small part of what was a thousand years ago.");
    }

    public void w2BtnWarrior() throws IOException {
        characterClass = 1;
        w2ImageClass.setImage(new Image(getClass().getResource("img/warrior.jpg").toExternalForm()));
        w2LabelClassDescription.setText("Warriors are a very powerful class, with the ability to tank or deal significant melee offensivePower.");
    }

    public void w2BtnRogue() throws IOException {
        characterClass = 2;
        w2ImageClass.setImage(new Image(getClass().getResource("img/rogue.jpg").toExternalForm()));
        w2LabelClassDescription.setText("Rogues are a leather-clad melee class capable of dealing large amounts of offensivePower to their enemies with a flurry of very fast attacks.");
    }

    public void w2BtnStart() throws IOException {
        if (!w2TextFieldName.getText().isEmpty()) {
            hero.name = w2TextFieldName.getText();
            new Switcher(stage, "MapScreen.fxml", "sandboxFX w3");
        } else {
            Alert onEmptyName = new Alert(Alert.AlertType.INFORMATION);
            onEmptyName.setTitle("Info");
            onEmptyName.setContentText("Please enter character name.");
            onEmptyName.setHeaderText(null);
            onEmptyName.showAndWait();
        }
    }

    //    Window3   MapScreen

    private void showDescription() {
        mapLocationDescriptionGrid.add(new Label("   Forest [1]"), 0, 0);
        mapLocationDescriptionGrid.add(new Label("   Enemies: \n    Wolf\n    Bear\n    Boar"), 0, 1);
        mapLocationDescriptionGrid.getChildren().forEach(x -> x.setStyle("-fx-text-fill: white"));
        mapLocationDescriptionGrid.setStyle("-fx-background-color: rgba(0, 0, 0, 0.75)");
    }

    public void hideDescription() {
        mapLocationDescriptionGrid.getChildren().clear();
        mapLocationDescriptionGrid.setStyle("-fx-background-color: rgba(0, 0, 0, 0.0)");
    }

    public void mapLocation1Entered() {
        showDescription();
    }

    public void location1() throws IOException {
        Enemy.setLocation(1, 4, new String[]{"Wolf", "Bear", "Boar"});
        goBattle();
    }

    public void location2() throws IOException {
        Enemy.setLocation(4, 7, new String[]{"Goblin", "Kobold", "Beorn"});
        goBattle();
    }

    public void location3() throws IOException {
        Enemy.setLocation(7, 17, new String[]{"Bandit", "Juggernaut", "Theif"});
        goBattle();
    }

    private void goBattle() throws IOException {
        new Switcher(stage, "BattleScreen.fxml", "sandboxFX w4");
    }

    public void goCharacter() throws IOException {
        new Switcher(stage, "CharacterScreen.fxml", "sandboxFX w5");
    }

    public void goTown() throws IOException {
        new Switcher(stage, "TownScreen.fxml", "sandboxFX w6");
    }

    //    Window4   BattleScreen

    void initBattleScreen() {
        w4Skills.setVisible(false);
        w4ImgBack.setVisible(true);
        getHeroInfo();
        enemyHP.setText("");
        w4Enemy1.setText(Enemy.enemies.get(0).name + " [" + Enemy.enemies.get(0).level + "]");
        w4Enemy2.setText(Enemy.enemies.get(1).name + " [" + Enemy.enemies.get(1).level + "]");
        w4Enemy3.setText(Enemy.enemies.get(2).name + " [" + Enemy.enemies.get(2).level + "]");
        w4Enemy4.setText(Enemy.enemies.get(3).name + " [" + Enemy.enemies.get(3).level + "]");
        w4Enemy5.setText(Enemy.enemies.get(4).name + " [" + Enemy.enemies.get(4).level + "]");
        w4Enemy6.setText(Enemy.enemies.get(5).name + " [" + Enemy.enemies.get(5).level + "]");
        w4Enemy7.setText(Enemy.enemies.get(6).name + " [" + Enemy.enemies.get(6).level + "]");
        w4Enemy8.setText(Enemy.enemies.get(7).name + " [" + Enemy.enemies.get(7).level + "]");
        w4Enemy9.setText(Enemy.enemies.get(8).name + " [" + Enemy.enemies.get(8).level + "]");
        w4Enemy10.setText(Enemy.enemies.get(9).name + " [" + Enemy.enemies.get(9).level + "]");
        w4Enemy11.setText(Enemy.enemies.get(10).name + " [" + Enemy.enemies.get(10).level + "]");
        w4Enemy12.setText(Enemy.enemies.get(11).name + " [" + Enemy.enemies.get(11).level + "]");
    }

    public void attackEnemy1() {
        enemy = Enemy.enemies.get(0);
        w4Enemy1.setDisable(true);
        initBattle();
    }

    public void attackEnemy2() {
        enemy = Enemy.enemies.get(1);
        w4Enemy2.setDisable(true);
        initBattle();
    }

    public void attackEnemy3() {
        enemy = Enemy.enemies.get(2);
        w4Enemy3.setDisable(true);
        initBattle();
    }

    public void attackEnemy4() {
        enemy = Enemy.enemies.get(3);
        w4Enemy4.setDisable(true);
        initBattle();
    }

    public void attackEnemy5() {
        enemy = Enemy.enemies.get(4);
        w4Enemy5.setDisable(true);
        initBattle();
    }

    public void attackEnemy6() {
        enemy = Enemy.enemies.get(5);
        w4Enemy6.setDisable(true);
        initBattle();
    }

    public void attackEnemy7() {
        enemy = Enemy.enemies.get(6);
        w4Enemy7.setDisable(true);
        initBattle();
    }

    public void attackEnemy8() {
        enemy = Enemy.enemies.get(7);
        w4Enemy8.setDisable(true);
        initBattle();
    }

    public void attackEnemy9() {
        enemy = Enemy.enemies.get(8);
        w4Enemy9.setDisable(true);
        initBattle();
    }

    public void attackEnemy10() {
        enemy = Enemy.enemies.get(9);
        w4Enemy10.setDisable(true);
        initBattle();
    }

    public void attackEnemy11() {
        enemy = Enemy.enemies.get(10);
        w4Enemy11.setDisable(true);
        initBattle();
    }

    public void attackEnemy12() {
        enemy = Enemy.enemies.get(11);
        w4Enemy12.setDisable(true);
        initBattle();
    }

    private void initBattle() {
        w4Skills.setVisible(true);
        w4ImgBack.setVisible(false);
        w4ImgNext.setVisible(false);
        w4MobGrid.getChildren().forEach(x -> x.setVisible(false));
        w4BattleLog.setVisible(true);
//        hero.name = "Saber"; //todo: delete after debug
//        AbilityPanel.initAbilityPanel();
//        abilityPanel.getChildren().addAll(new Label("Name: "));
        w4BattleLog.clear();
        w4BattleLog.appendText(String.format("Battle between %s and %s has been stated.", hero.name, enemy.name));
        updateBattleScreen();
    }

    private void getHeroInfo() {
        heroHP.setText(hero.hpCur + " / " + hero.hpMax);
        heroHPbar.setProgress((double) hero.hpCur / (double) hero.hpMax);
//        enemyHPbar.setProgress((double) enemy.hpCur / (double) enemy.hpMax);
        w4HeroStat1.setText(String.format("%s [%s : level %s]", hero.name, hero.heroClass.name, hero.level));
        w4HeroStat2.setText("Experience: " + hero.expCur + " / " + hero.expMax);
        w4HeroStat3.setText("Attack: " + hero.attack);
        w4HeroStat4.setText("Hit %: " + (60 + (((hero.level - enemy.level) * 2) + ((hero.offensivePower - enemy.defensivePower) / 2))));
        w4HeroStat5.setText("Offensive Power: " + hero.offensivePower);
        w4HeroStat6.setText("Defensive Power: " + hero.defensivePower);
    }

    private void getEnemyInfo() {
        enemyHP.setText(enemy.hpCur + " / " + enemy.hpMax);
        enemyHPbar.setProgress((double) enemy.hpCur / (double) enemy.hpMax);
        w4ImageEnemy.setImage(new Image(getClass().getResource(String.format("img/enemies/%s.jpg", enemy.name.toLowerCase())).toExternalForm()));
        w4EnemyStat1.setText(String.format("%s [level %s]", enemy.name, enemy.level));
        w4EnemyStat2.setText("");
        w4EnemyStat3.setText("Attack: " + enemy.attack);
        w4EnemyStat4.setText("Hit %: " + (60 + (((enemy.level - hero.level) * 2) + ((enemy.offensivePower - hero.defensivePower) / 2))));
        w4EnemyStat5.setText("Offensive Power: " + enemy.offensivePower);
        w4EnemyStat6.setText("Defensive Power: " + enemy.defensivePower);
    }

    private void updateBattleScreen() {
        getHeroInfo();
        w4BattleLog.appendText(Helper.buffer);
        getEnemyInfo();

        if (enemy.hpCur <= 0) {
            w4ImgBack.setVisible(true);
            w4ImgNext.setVisible(true);
        }
    }

    public void showEnemyDesc(MouseEvent mouseEvent) {
        w4ImageEnemy.setVisible(true);
        w4VBox2.getChildren().forEach(e -> e.setVisible(true));
        String s = mouseEvent.getSource().toString();
        enemy = Enemy.enemies.get(Integer.valueOf(s.substring(17, s.indexOf(','))) - 1);
        getEnemyInfo();
        getHeroInfo();
    }

    public void hideEnemyDesc(MouseEvent mouseEvent) {
        if (!mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
            enemyHP.setText("");
            w4ImageEnemy.setVisible(false);
            w4VBox2.getChildren().forEach(e -> e.setVisible(false));
        }
    }

    public void w4ImgSkill1(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.SECONDARY) {
//            mouseEvent.consume();
            hero.hpCur = hero.hpMax;
            updateBattleScreen();
        } else {
            w4BattleLog.appendText(String.format("\n\nRound %s.", ++round));
            hero.hit(enemy);
            updateBattleScreen();

            if (enemy.hpCur > 0) {
                enemy.hit(hero);
                updateBattleScreen();
            }
        }
    }

    public void w4ImgSkill2() {
    }

    public void w4ImgSkill3() {
    }

    public void w4ImgSkill4() {
        hero.level++;
        updateBattleScreen();
        System.out.println("Lvl up."); //todo: delete
    }

    public void w4ImgSkill5(MouseEvent mouseEvent) { //todo: on right click appears vbox/grid with skills/consumables
        /*if (mouseEvent.getButton() == MouseButton.PRIMARY) {//todo: delete all
            if (Helper.skill5 != null) {
                System.out.println("tratata");
                Helper.skill5.useItem();
                updateBattleScreen();
            }
        }
        if (mouseEvent.getButton() == MouseButton.SECONDARY) {
            showSkillGrid(mouseEvent);
        }*/
    }

/*    public void showSkillGrid(MouseEvent mouseEvent) {//todo: delete
        skillSelectionGrid.setVisible(true);
        skillSelectionGrid.setMouseTransparent(false);
        Helper.skillGridList.clear();
        skillSelectionGrid.setStyle("-fx-background-color: rgba(0, 0, 0, 0.9);-fx-grid-lines-visible: true;");
        int col = 0;
        int row = 0;

        for (Item i : Hero.inventory) {
            if (i instanceof Consumable) {
                skillSelectionGrid.add(generateIcon(i), col, row);
                Helper.skillGridList.add(i);
                col++;
            }
        }
//        System.out.println(mouseEvent.getSource().toString());
//     handleSkillGrid(mouseEvent);

    }

    public void handleSkillGrid(MouseEvent mouseEvent) {//todo: delete
//        System.out.println("\n\n1:   " + mouseEvent.toString());
        switch (mouseEvent.getEventType().toString()) {
//            case "MOUSE_MOVED":
//                switch (mouseEvent.getSource().toString()) {
//                    case "Pane[id=w5WeaponBox]":
//                        if (hero.equipped.containsKey(ItemType.WEAPON))
//                            showItemDescription(hero.equipped.get(ItemType.WEAPON), mouseEvent.getX() + w5WeaponBox.getLayoutX() + 50, mouseEvent.getY() + w5WeaponBox.getLayoutY() + 65);
//                        break;
//                }
//                break;
            case "MOUSE_PRESSED":
                int index = (int) mouseEvent.getX() / 60 + (int) (mouseEvent.getY() / 60) * 5;
                System.out.println(index);
                if (Helper.skillGridList.size() - 1 <= index) {
                    System.out.println(Helper.skillGridList.get(index));
                    Helper.skill5 = (Consumable) Helper.skillGridList.get(index);
                    skillSelectionGrid.setVisible(false);

                }


        }
//                break;
//            case "MOUSE_EXITED":
//                switch (mouseEvent.getSource().toString()) {
//                    case "Pane[id=w5WeaponBox]":
//                        hideItemDescription();
//                        break;
//                    case "Pane[id=w5ChestBox]":
//                        hideItemDescription();
//                        break;
//        }
    }*/

    public void w4ImgSkill6(MouseEvent mouseEvent) {
        if (mouseEvent.getButton() == MouseButton.SECONDARY) {
//            mouseEvent.consume();
            hero.hpCur = hero.hpMax;
            updateBattleScreen();
        } else {
            w4BattleLog.appendText(String.format("\n\nRound %s.", ++round));
            hero.hit(enemy);
            updateBattleScreen();

            if (enemy.hpCur > 0) {
                enemy.hit(hero);
                updateBattleScreen();
            }
        }
    }

    public void w4ImgBack() throws IOException {
        Helper.buffer = "";
        new Switcher(stage, "MapScreen.fxml", "sandboxFX w3");
    }

    public void w4ImgNext(MouseEvent mouseEvent) throws IOException {
        Helper.buffer = "";
        w4Skills.setVisible(false);
        w4ImgBack.setVisible(true);
        w4ImgNext.setVisible(false);
        w4MobGrid.getChildren().forEach(x -> x.setVisible(true));
        w4BattleLog.setVisible(false);
        enemyHP.setText("");
        w4ImageEnemy.setVisible(false);
        w4VBox2.getChildren().forEach(e -> e.setVisible(false));
    }

    //    Window5 CharacterScreen

    private int inventoryPage = 0;

    void initCharacterScreen() {
        updateCharacterScreen();
    }

    private void updateCharacterScreen() {
        w5Name.setText(hero.name);
        w5Class.setText(hero.heroClass.name);
        w5Level.setText(String.valueOf("Level   " + hero.level));
        w5Experience.setText("Experience   " + hero.expCur + " / " + hero.expMax);
        w5EnemyCounter.setText(String.valueOf("Enemies defeated   " + hero.enemyCounter));
        w5Gold.setText(String.valueOf("Gold   " + hero.gold));
        w5Strength.setText(String.valueOf(hero.strength));
        w5Agility.setText(String.valueOf(hero.agility));
        w5Vitality.setText(String.valueOf(hero.vitality));
        w5Intellect.setText(String.valueOf(hero.intellect));
        w5Attack.setText(String.valueOf(hero.attack));
        w5Armor.setText(String.valueOf(hero.armor));
        w5Offensive.setText(String.valueOf(hero.offensivePower));
        w5Defensive.setText(String.valueOf(hero.defensivePower));
        w5HP.setText(String.valueOf(hero.hpCur + "/" + hero.hpMax));
        w5MP.setText(String.valueOf(hero.mpCur + "/" + hero.mpMax));
        w5LearningPoints.setText(String.valueOf(hero.learningPoints));
        w5SkillPoints.setText(String.valueOf(hero.skillPoints));

        if (hero.equipped.containsKey(ItemType.CHEST)) {
            w5ChestBox.getChildren().clear();
            w5ChestBox.getChildren().add(generateIcon(hero.equipped.get(ItemType.CHEST)));
//            w5ChestBox.getChildren().add(hero.equipped.get(ItemType.CHEST).pane);
        } else w5ChestBox.getChildren().clear(); //w5WeaponBox.setBackground(null);
        if (hero.equipped.containsKey(ItemType.WEAPON)) {
            w5WeaponBox.getChildren().clear();
//            w5WeaponBox.getChildren().add(hero.equipped.get(ItemType.WEAPON).pane);
            w5WeaponBox.getChildren().add(generateIcon(hero.equipped.get(ItemType.WEAPON)));
        } else w5WeaponBox.getChildren().clear(); //w5WeaponBox.setBackground(null);

        if (hero.learningPoints > 0) {
            w5SkillUp1.setVisible(true);
            w5SkillUp2.setVisible(true);
            w5SkillUp3.setVisible(true);
            w5SkillUp4.setVisible(true);
        } else {
            w5SkillUp1.setVisible(false);
            w5SkillUp2.setVisible(false);
            w5SkillUp3.setVisible(false);
            w5SkillUp4.setVisible(false);
        }
        getInventory(0);
    }

    private void getInventory(Integer inventoryPage) { //todo: make method inventory.size dependent!
        inventoryGrid.getChildren().clear();
        hero.inventorySort(Hero.inventory);
        int i = inventoryPage;
        for (int row = 0; row < 6; row++) {
            for (int col = 0; col < 5; col++, i++) {
                if (i < Hero.inventory.size()) inventoryGrid.add(generateIcon(Hero.inventory.get(i)), col, row);
            }
        }
        if (Hero.inventory.size() > inventoryPage + 30) inventoryNext.setVisible(true);
        else inventoryNext.setVisible(false);
        if (inventoryPage > 0) inventoryBack.setVisible(true);
        else inventoryBack.setVisible(false);
    }

    private Pane generateIcon(Item i) {
        Pane p = new Pane(new ImageView(getClass().getResource(i.img).toExternalForm()));
        if (i instanceof Consumable) {
            Label stackCounter = new Label(String.valueOf(((Consumable) i).stackCounter));
            stackCounter.setPadding(new Insets(3, 0, 0, 3));
            p.getChildren().add(stackCounter);
        }
        switch (i.getItemQuality()) {
            case NORMAL:
                p.setStyle("-fx-background-color: White");
                break;
            case MAGIC:
                p.setStyle("-fx-background-color: DodgerBlue");
                break;
            case RARE:
                p.setStyle("-fx-background-color: Yellow");
                break;
            case EPIC:
                p.setStyle("-fx-background-color: DarkOrchid");
                break;
        }
        return p;
    }

    public void inventoryGridMoved(MouseEvent mouseEvent) {
        int index = ((int) mouseEvent.getX() / 60 + (int) (mouseEvent.getY() / 60) * 5) + inventoryPage;
        if (index < Hero.inventory.size())
            showItemDescription(Hero.inventory.get(index), mouseEvent.getX() + inventoryGrid.getLayoutX() - 150, mouseEvent.getY() + inventoryGrid.getLayoutY() - 65);
        else hideItemDescription();
    }

    private void showItemDescription(Item item, Double layX, Double layY) {
        w5ItemDescription.getChildren().clear();
        w5ItemDescription.setLayoutX(layX);
        w5ItemDescription.setLayoutY(layY);
        Label itemName = new Label(item.name);
        Label itemDescription = new Label(item.description);
        itemDescription.setTextAlignment(TextAlignment.CENTER);
        itemDescription.setWrapText(true);
        switch (item.getItemQuality()) {
            case NORMAL:
                itemName.setStyle("-fx-text-fill: White");
                break;
            case MAGIC:
                itemName.setStyle("-fx-text-fill: DodgerBlue");
                break;
            case RARE:
                itemName.setStyle("-fx-text-fill: Yellow");
                break;
            case EPIC:
                itemName.setStyle("-fx-text-fill: DarkOrchid");
                break;
        }
        w5ItemDescription.setPrefHeight(w5ItemDescription.getMinHeight());
        w5ItemDescription.getChildren().addAll(itemName, itemDescription);
        w5ItemDescription.setStyle("-fx-background-color: rgba(0, 0, 0, 0.9)");
    }

    public void hideItemDescription() {
        w5ItemDescription.getChildren().clear();
        w5ItemDescription.setStyle("-fx-background-color: rgba(0, 0, 0, 0.0)");
    }

    public void w5InventoryClicked(MouseEvent mouseEvent) {
        int index = (int) mouseEvent.getX() / 60 + (int) (mouseEvent.getY() / 60) * 5;
        if (index < Hero.inventory.size()) {
            ((UsableItem) Hero.inventory.get(index)).useItem();
            updateCharacterScreen();
            if (index < Hero.inventory.size())
                showItemDescription(Hero.inventory.get(index), mouseEvent.getX() + inventoryGrid.getLayoutX() - 150, mouseEvent.getY() + inventoryGrid.getLayoutY() - 65);
            else hideItemDescription();
        }
    }

    public void inventoryTest(MouseEvent mouseEvent) { //todo: delete this + delete from CharacterScreen.fxml after test
        hero.addItems(10);
        updateCharacterScreen();
    }

    public void inventoryNext(ActionEvent actionEvent) {
        if (Hero.inventory.size() > inventoryPage + 30) {
            inventoryPage += 30;
        }
        getInventory(inventoryPage);
    }

    public void inventoryBack(ActionEvent actionEvent) {
        if (inventoryPage != 0) inventoryPage -= 30;
        getInventory(inventoryPage);
    }

    public void handleEquipment(MouseEvent mouseEvent) {
        switch (mouseEvent.getEventType().toString()) {
            case "MOUSE_MOVED":
                switch (mouseEvent.getSource().toString()) {
                    case "Pane[id=w5WeaponBox]":
                        if (hero.equipped.containsKey(ItemType.WEAPON))
                            showItemDescription(hero.equipped.get(ItemType.WEAPON), mouseEvent.getX() + w5WeaponBox.getLayoutX() + 50, mouseEvent.getY() + w5WeaponBox.getLayoutY() + 65);
                        break;
                    case "Pane[id=w5ChestBox]":
                        if (hero.equipped.containsKey(ItemType.CHEST))
                            showItemDescription(hero.equipped.get(ItemType.CHEST), mouseEvent.getX() + w5ChestBox.getLayoutX() + 50, mouseEvent.getY() + w5ChestBox.getLayoutY() + 65);
                        break;
                }
                break;
            case "MOUSE_PRESSED":
                switch (mouseEvent.getSource().toString()) {
                    case "Pane[id=w5WeaponBox]":
                        if (hero.equipped.containsKey(ItemType.WEAPON)) {
                            hero.unequipItem(hero.equipped.get(ItemType.WEAPON));
                            hideItemDescription();
                            updateCharacterScreen();
                        }
                        break;
                    case "Pane[id=w5ChestBox]":
                        if (hero.equipped.containsKey(ItemType.CHEST)) {
                            hero.unequipItem(hero.equipped.get(ItemType.CHEST));
                            hideItemDescription();
                            updateCharacterScreen();
                        }
                        break;
                }
                break;
            case "MOUSE_EXITED":
                switch (mouseEvent.getSource().toString()) {
                    case "Pane[id=w5WeaponBox]":
                        hideItemDescription();
                        break;
                    case "Pane[id=w5ChestBox]":
                        hideItemDescription();
                        break;
                }
        }
    }

    public void goMap() throws IOException {
        new Switcher(stage, "MapScreen.fxml", "sandboxFX w3");
    }

    public void w5SkillUp1() {
        hero.spendStatPointsFX("STR");
        updateCharacterScreen();
    }

    public void w5SkillUp2() {
        hero.spendStatPointsFX("AGI");
        updateCharacterScreen();
    }

    public void w5SkillUp3() {
        hero.spendStatPointsFX("VIT");
        updateCharacterScreen();
    }

    public void w5SkillUp4() {
        hero.spendStatPointsFX("INT");
        updateCharacterScreen();
    }

    //    Window6   TownScreen

    public void goShop() throws IOException {
        new Switcher(stage, "ShopScreen.fxml", "sandboxFX w7");
    }

    public void goChurch() throws IOException {
        new Switcher(stage, "ChurchScreen.fxml", "sandboxFX w8");
    }

    public void goTavern() {
    }

    //    Window7   ShopScreen

    void initShopScreen() {
        Helper.shopList.clear();
        int shopSize = Helper.getRandom(15, 30);
        for (int i = 0; i < shopSize; i++) Helper.shopList.add(Hero.generateRandomEquipableItem());
        updateShopScreen();
    }

    private void updateShopScreen() {
        w7Gold.setText("Gold: " + hero.gold);
        getInventory(0);
        getShop();
    }

    private void getShop() {
        shopGrid.getChildren().clear();
        int i = 0;
        for (int row = 0; row < 6; row++) {
            for (int col = 0; col < 5; col++, i++)
                if (i < Helper.shopList.size()) {
                    hero.inventorySort(Helper.shopList);
                    shopGrid.add(generateIcon(Helper.shopList.get(i)), col, row);
                }
        }
    }

    public void sellItem(MouseEvent mouseEvent) {
        int index = (int) mouseEvent.getX() / 60 + (int) (mouseEvent.getY() / 60) * 5;
//        Item i = Hero.inventory.get(index);
//        if (i instanceof Consumable) i.cost *= ((Consumable) i).stackCounter;
        if (Hero.inventory.size() > index && mouseEvent.isPrimaryButtonDown()) {
            Alert a = new Alert(Alert.AlertType.CONFIRMATION);
            a.setTitle("Sell");
            a.setHeaderText(null);
            a.setGraphic(null);
            a.setContentText(String.format("Sell [%s] for %s gold?", Hero.inventory.get(index).name, Hero.inventory.get(index).cost));
            a.showAndWait();
            if (a.getResult().toString().equals("ButtonType [text=OK, buttonData=OK_DONE]")) {
                hero.gold += Hero.inventory.get(index).cost;
                Helper.shopList.add(Hero.inventory.get(index));
                Hero.inventory.remove(index);
                updateShopScreen();
            }
        } else if (Hero.inventory.size() > index && mouseEvent.isSecondaryButtonDown()) { //duplicates the code above
            hero.gold += Hero.inventory.get(index).cost;
            Helper.shopList.add(Hero.inventory.get(index));
            Hero.inventory.remove(index);
            updateShopScreen();
        }
    }

    public void buyItem(MouseEvent mouseEvent) {
        int index = (int) mouseEvent.getX() / 60 + (int) (mouseEvent.getY() / 60) * 5;
        Alert a = new Alert(Alert.AlertType.CONFIRMATION);
        a.setTitle("Buy");
        a.setHeaderText(null);
        a.setGraphic(null);
        if (Helper.shopList.size() > index && mouseEvent.getButton().toString().equals("PRIMARY")) {
            if (hero.gold >= Helper.shopList.get(index).cost) {
                a.setContentText(String.format("Buy [%s] for %s gold?", Helper.shopList.get(index).name, Helper.shopList.get(index).cost));
                a.showAndWait();
                if (a.getResult().toString().equals("ButtonType [text=OK, buttonData=OK_DONE]")) {
                    hero.gold -= Helper.shopList.get(index).cost;
                    Hero.inventory.add(Helper.shopList.get(index));
                    Helper.shopList.remove(index);
                    updateShopScreen();
                }
            } else {
                a.setAlertType(Alert.AlertType.INFORMATION);
                a.setContentText("You don't have enough gold.");
                a.showAndWait();
            }
        }
    }

    public void shopGridMoved(MouseEvent mouseEvent) {
        int index = (int) mouseEvent.getX() / 60 + (int) (mouseEvent.getY() / 60) * 5;
        if (index < Helper.shopList.size())
            showItemDescription(Helper.shopList.get(index), mouseEvent.getX() + shopGrid.getLayoutX() + 10, mouseEvent.getY() + shopGrid.getLayoutY() - 65);
        else hideItemDescription();
    }

    //    Window8   ChurchScreen

    void initChurchScreen() {
        updateChurchScreen();
    }

    private void updateChurchScreen() {
        heroHP.setText(hero.hpCur + " / " + hero.hpMax);
        heroHPbar.setProgress((double) hero.hpCur / (double) hero.hpMax);
    }

    public void churchPray() {
        for (int i = 0; i <= hero.hpMax / 20; i++) {
            if (hero.hpCur < hero.hpMax) hero.hpCur++;
        }
        updateChurchScreen();
    }

//    public void w5TEST(ActionEvent actionEvent) { //todo: delete after Consumable debug
//        Item nitem = Hero.generateRandomConsumable();
//        if (nitem instanceof Consumable) for (Item i : Hero.inventory) {
//            if (i.equals(nitem)) {
//                ((Consumable) Hero.inventory.get(Hero.inventory.indexOf(i))).stackCounter += 250;
//                updateCharacterScreen();
//                return;
//            }
//        }
//        Hero.inventory.add(nitem);
//        updateCharacterScreen();
//    }
}