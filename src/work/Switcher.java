package work;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

class Switcher {
    Switcher(Stage stage, String sceneName, String windowName) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(sceneName));
        Parent root = loader.load();
        Controller controller = loader.getController();

        stage.setTitle(windowName);
        stage.setWidth(804);
        stage.setHeight(628);
        stage.setResizable(false);

        Scene scene = new Scene(root);
        scene.getStylesheets().addAll(this.getClass().getResource("style.css").toExternalForm());
        stage.setScene(scene);
        stage.show();

        controller.setStage(stage);

        switch (sceneName) {
            case "BattleScreen.fxml":
                controller.initBattleScreen();
                break;
            case "CharacterScreen.fxml":
                controller.initCharacterScreen();
                break;
            case "ShopScreen.fxml":
                controller.initShopScreen();
                break;
            case "ChurchScreen.fxml":
                controller.initChurchScreen();
                break;
        }
    }
}