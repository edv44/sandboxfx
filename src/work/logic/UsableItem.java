package work.logic;

public abstract class UsableItem extends Item {
    UsableItem() {
        super();
    }

    public void useItem() {
    }
}
