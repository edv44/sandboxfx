package work.logic;

abstract class Armor extends EquipableItem {

    Armor() {
        super();
        primaryBonuses.add(StatType.STRENGTH); //todo:new object shouldn't have this list in the parameters
        primaryBonuses.add(StatType.AGILITY);
        primaryBonuses.add(StatType.VITALITY);
        primaryBonuses.add(StatType.INTELLECT);
    }

    void getPrimaryStat(int roll) {
        StatType s = primaryBonuses.get(Helper.getRandom(0, primaryBonuses.size() - 1));
        itemStats.put(s, roll);
        primaryBonuses.remove(s);
    }

    String getStatDesc() {
        String statDesc = itemType.toString().substring(0, 1) + itemType.toString().toLowerCase().substring(1, getItemType().toString().length());
        statDesc += "\n\nArmor: " + itemStats.get(StatType.ARMOR) + "\n";
        if (itemStats.containsKey(StatType.STRENGTH))
            statDesc += "\nSTR: " + itemStats.get(StatType.STRENGTH).toString();
        if (itemStats.containsKey(StatType.AGILITY))
            statDesc += "\nAGI: " + itemStats.get(StatType.AGILITY).toString();
        if (itemStats.containsKey(StatType.VITALITY))
            statDesc += "\nVIT: " + itemStats.get(StatType.VITALITY).toString();
        if (itemStats.containsKey(StatType.INTELLECT))
            statDesc += "\nINT: " + itemStats.get(StatType.INTELLECT);
        return statDesc;
    }
}

class Chest extends Armor {

    Chest() {
        super();
        img = "img/items/armor.png";
        int roll = Helper.getRandom(1, Hero.getInstance().level);//todo: roll 1..(enemy.lvl - hero.lvl)

        switch (roll) {
            case 1:
                name = "Quilted Armor";
                break;
            case 2:
                name = "Leather armor";
                break;
            case 3:
                name = "Hard Leather Armor";
                break;
            case 4:
                name = "Studded Leather";
                break;
            case 5:
                name = "Ring Mail";
                break;
            case 6:
                name = "Scale Mail";
                break;
            case 7:
                name = "Breast Plate";
                break;
            case 8:
                name = "Chain Mail";
                break;
            case 9:
                name = "Splint Mail";
                break;
            case 10:
                name = "Light Plate";
                break;
            case 11:
                name = "Field Plate";
                break;
            case 12:
                name = "Plate Mail";
                break;
            case 13:
                name = "Gothic Plate";
                break;
            case 14:
                name = "Full Plate Mail";
                break;
            case 15:
            default:
                name = "Ancient Armor";
                break;
        }

        itemType = ItemType.CHEST;
        setItemQuality(generateItemQuality());
        itemStats.put(StatType.ARMOR, roll);

        switch (getItemQuality()) {
            case NORMAL:
                cost = roll * 30;
                break;
            case MAGIC:
                this.getPrimaryStat(roll);
                cost = roll * 60;
                break;
            case RARE:
                this.getPrimaryStat(roll);
                this.getPrimaryStat(roll);
                cost = roll * 90;
                break;
            case EPIC:
                this.getPrimaryStat(roll);
                this.getPrimaryStat(roll);
                this.getPrimaryStat(roll);
                cost = roll * 120;
                break;
        }
        description = String.format("%s\n\nCost: %s", getStatDesc(), cost);
    }
}