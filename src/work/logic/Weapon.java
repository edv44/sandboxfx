package work.logic;

abstract class Weapon extends EquipableItem {

    Weapon() {
        super();
        itemType = ItemType.WEAPON;
        primaryBonuses.add(StatType.STRENGTH); //todo:new object shouldn't have this list in the parameters
        primaryBonuses.add(StatType.AGILITY);
        primaryBonuses.add(StatType.VITALITY);
        primaryBonuses.add(StatType.INTELLECT);
    }

    void getPrimaryStat(int roll) {
        StatType s = primaryBonuses.get(Helper.getRandom(0, primaryBonuses.size() - 1));
        itemStats.put(s, roll);
        primaryBonuses.remove(s);
    }

    String getStatDesc() {
        String statDesc = itemType.toString().substring(0, 1) + itemType.toString().toLowerCase().substring(1, getItemType().toString().length());
        statDesc += "\n\nDamage: " + itemStats.get(StatType.ATTACK) + "\n";
        if (itemStats.containsKey(StatType.STRENGTH))
            statDesc += "\nSTR: " + itemStats.get(StatType.STRENGTH).toString();
        if (itemStats.containsKey(StatType.AGILITY))
            statDesc += "\nAGI: " + itemStats.get(StatType.AGILITY).toString();
        if (itemStats.containsKey(StatType.VITALITY))
            statDesc += "\nVIT: " + itemStats.get(StatType.VITALITY).toString();
        if (itemStats.containsKey(StatType.INTELLECT))
            statDesc += "\nINT: " + itemStats.get(StatType.INTELLECT);
        return statDesc;
    }
}

class Sword extends Weapon {

    Sword() {
        super();
        img = "img/items/weapon.png";

        int roll = Helper.getRandom(1, Hero.getInstance().level); //todo: roll 1..(enemy.lvl - hero.lvl)

        switch (roll) {
            case 1:
            case 2:
                name = "Short Sword";
                break;
            case 3:
            case 4:
                name = "Scimitar";
                break;
            case 5:
            case 6:
                name = "Sabre";
                break;
            case 7:
            case 8:
                name = "Falchion";
                break;
            case 9:
            case 10:
                name = "Crystal Sword";
                break;
            case 11:
            case 12:
                name = "Broad Sword";
                break;
            case 13:
            case 14:
                name = "Long Sword";
                break;
            case 15:
            default:
                name = "War Sword";
                break;
        }

        setItemQuality(generateItemQuality());
        itemStats.put(StatType.ATTACK, roll * 3);

        switch (getItemQuality()) {
            case NORMAL:
                cost = roll * 30;
                break;
            case MAGIC:
                this.getPrimaryStat(roll);
                cost = roll * 60;
                break;
            case RARE:
                this.getPrimaryStat(roll);
                this.getPrimaryStat(roll);
                cost = roll * 90;
                break;
            case EPIC:
                this.getPrimaryStat(roll);
                this.getPrimaryStat(roll);
                this.getPrimaryStat(roll);
                cost = roll * 120;
                break;
        }
        description = String.format("%s\n\nCost: %s", getStatDesc(), cost);
    }
}