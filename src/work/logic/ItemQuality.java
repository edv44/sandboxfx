package work.logic;

public enum ItemQuality {
    NORMAL, MAGIC, RARE, EPIC
}
