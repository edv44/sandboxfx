package work.logic;

public class Consumable extends UsableItem { //heal potion, exp scroll, item chest(quality/type)
    public int stackCounter;
    public int baseCost;

    Consumable() {
        super();
        itemType = ItemType.CONSUMABLE;
    }
}

class HealPotion extends Consumable {
    private int adjustHp;

    HealPotion(int healPotLvl) {
        super();
        isStackable = true;
        img = "img/items/healpotion1.png";
        setItemQuality(ItemQuality.NORMAL);
        stackCounter = 1;

        switch (healPotLvl) {
            case 1:
                name = "Minor heal potion";
                adjustHp = 30;
                break;
            case 2:
                name = "Lesser heal potion";
                adjustHp = 60;
                break;
            case 3:
                name = "Heal potion";
                adjustHp = 90;
                break;
            default:
                System.out.println("HealPotion() default");
                break;
        }
        baseCost = cost = adjustHp;
        description = "Consumable\n\nReplenishes your health by " + adjustHp + " hp.\n\nCost: " + cost + ".";
    }

    @Override
    public void useItem() {
        super.useItem();
        Hero.getInstance().hpCur += adjustHp;
        if (Hero.getInstance().hpCur > Hero.getInstance().hpMax) Hero.getInstance().hpCur = Hero.getInstance().hpMax;
        for (Item i : Hero.inventory)
            if (i.equals(this)) {
                if (((Consumable) i).stackCounter != 1) ((Consumable) i).stackCounter--;
                else {
                    Hero.inventory.remove(this);
                    return;
                }
            }
    }
}