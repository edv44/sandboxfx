package work.logic;

public interface IAttackable {
    void hit(Character target);
}