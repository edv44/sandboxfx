package work.logic;

public abstract class Character implements IAttackable {
    public String name;
    public int hpCur;
    public int hpMax;
    public int offensivePower;
    public int defensivePower;
    public int attack;
    public int armor;
    public int level;
    public int expCur;
    public int gold;

    @Override
    public void hit(Character target) {
//        int chanceToHit = 100 * defensivePower / (defensivePower + target.armor) * 2 * level / (level + target.level);
//        int chanceToHit = offensivePower/target.defensivePower*10;
        int chanceToHit = 60 + (((level - target.level) * 2) + ((offensivePower - target.defensivePower) / 2));
        if (chanceToHit < 5) chanceToHit = 5;
        else if (chanceToHit > 95) chanceToHit = 95;

        if (Helper.getRandom(1, 100) <= chanceToHit) {
            if (attack - target.armor / 6 <= 0) attack = 0; //todo: rework with hit in hero's piece of armor instead of /6
            Helper.buffer = String.format("\n%s hits %s by %s damage, (hit %s%s).", name, target.name, attack - target.armor / 6, chanceToHit, "%"); //hit info
            target.adjustHealth(-(attack - target.armor / 6));
        } else Helper.buffer = String.format("\n%s is missed, (hit %s%s).", name, chanceToHit, "%");
    }

    private void adjustHealth(int amount) {
        hpCur += amount;
        if (hpCur > hpMax) hpCur = hpMax;
        if (hpCur <= 0) {
            hpCur = 0;
            death();
        }
    }

    protected abstract void death();
}