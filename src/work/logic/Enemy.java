package work.logic;

import java.util.ArrayList;

public class Enemy extends Character {
    private static int minLvl;
    private static int maxLvl;
    private static String[] names = {""};
    public static ArrayList<Enemy> enemies = new ArrayList<>();

    public static void setLocation(int minLvl, int maxLvl, String[] names) {
        Enemy.minLvl = minLvl;
        Enemy.maxLvl = maxLvl;
        Enemy.names = names;
        enemies.clear();
        for (int i = 0; i <= 11; i++) enemies.add(generate());
    }

    public static Enemy generate() {
        Enemy enemy = new Enemy();
        enemy.level = Helper.getRandom(minLvl, maxLvl);
        enemy.name = names[Helper.getRandom(0, names.length - 1)];
        enemy.calcStats();
        return enemy;
    }

    private void calcStats() { //todo: will be fully reworked
        offensivePower = level * Helper.getRandom(10, 13);
        defensivePower = level * Helper.getRandom(7, 10);
        attack = level * Helper.getRandom(10, 13);
//        armor = level * Helper.getRandom(3, 5);
        hpCur = hpMax = Helper.getRandom(31, 45) * level;
        expCur = defensivePower + Math.round(offensivePower + hpMax) / 2; //todo: wtf
        gold = (int) ((armor + Math.round(offensivePower + hpMax) / 2) * 1.15); //todo: wtf
    }

    @Override
    protected void death() {
        Helper.buffer = String.format("%s\n\n%s defeated %s!", Helper.buffer, Hero.getInstance().name, name);
        Hero.getInstance().enemyCounter++;
        Hero.getInstance().drop(this);
        Hero.getInstance().expUp(this.expCur);
    }
}