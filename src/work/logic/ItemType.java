package work.logic;

public enum ItemType {
    WEAPON, HELMET, CHEST, GLOVES, BELT, BOOTS, CONSUMABLE
}
