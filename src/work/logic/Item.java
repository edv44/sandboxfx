package work.logic;

public class Item {
    public String name;
    public int cost;
    public String img;
    ItemType itemType;
    private ItemQuality itemQuality;
    public String description = "";
    boolean isStackable;

    static ItemQuality generateItemQuality() {
        ItemQuality q;
        int x = Helper.getRandom(1, 1000);
        if (x <= 650) q = ItemQuality.NORMAL; // 65% white
        else if (x > 650 && x <= 850) q = ItemQuality.MAGIC; // 20% blue
        else if (x > 850 && x <= 950) q = ItemQuality.RARE; // 10% yellow
        else q = ItemQuality.EPIC; // 5% epic
        return q;
    }

    public ItemQuality getItemQuality() {
        return itemQuality;
    }

    void setItemQuality(ItemQuality itemQuality) {
        this.itemQuality = itemQuality;
    }

    public ItemType getItemType() {
        return itemType;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        final Item other = (Item) obj;
        return (this.name == null) ? other.name == null : this.name.equals(other.name);
    }
}

