package work.logic;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Helper {
    private static Random r = new Random();
    public static String buffer = "";
    public static ArrayList<Item> shopList = new ArrayList();
    public static ArrayList<Item> skillGridList = new ArrayList();//todo: delete
    public static Consumable skill5;//todo: delete
    static void threadSleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static int getRandom(int min, int max) {
        return r.nextInt((max - min) + 1) + min;
    }
}