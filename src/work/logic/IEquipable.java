package work.logic;

public interface IEquipable {
    void equipItem();
    void unequipItem();
}