package work.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public abstract class EquipableItem extends UsableItem {
    Map<StatType, Integer> itemStats = new HashMap<>();
    ArrayList<StatType> primaryBonuses = new ArrayList<>();

    EquipableItem() {
        super();
        isStackable = false;
    }

    @Override
    public void useItem() {
        super.useItem();
        if (Hero.getInstance().equipped.containsKey(itemType)) { // todo: looks like sometimes in this case items are get deleted
            Hero.getInstance().unequipItem(this);
        }
        Hero.getInstance().equipped.put(this.itemType, this); //add equipped item to equipped list
        Hero.inventory.remove(this); //remove equipped item from inventory list
        for (Map.Entry<StatType, Integer> item1 : this.itemStats.entrySet()) {
            for (Stat stat : Hero.getInstance().heroStats) {
                if (stat.type.equals(item1.getKey())) {
                    stat.addMod(this);
                }
            }
        }
        Hero.getInstance().calcStats();
    }
}