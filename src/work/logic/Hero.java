package work.logic;

import javafx.scene.control.Alert;
import work.Controller;

import javax.lang.model.type.ArrayType;
import java.util.*;

public class Hero extends Character {
    public int expMax;
    public int strength;
    public int agility;
    public int vitality;
    public int intellect;
    public int mpMax;
    public int mpCur;
    public int enemyCounter;
    public int learningPoints;
    public int skillPoints;
    public static ArrayList<Item> inventory = new ArrayList<>();
    public Map<ItemType, EquipableItem> equipped = new HashMap<>();
    //    private Map<StatType, Integer> heroStats = new HashMap<>();
//    private Map<Stat, Integer> heroStats = new HashMap<>();
    ArrayList<Stat> heroStats;

    public HeroClass heroClass;
    private static Hero instance;

    public static Hero getInstance() {
        if (instance == null) {
            instance = new Hero();
        }
        return instance;
    }

    private Hero() {
        instance = this;
        level = 1;
        expCur = 0;
        expMax = 180;

        switch (Controller.characterClass) {
            case 1:
                heroClass = new ClassWarrior() {
                };
                break;
            case 2:
                heroClass = new ClassRogue() {
                };
                break;
        }

        applyClass();
//        addItems(21); //todo:delete after test
    }

    private void applyClass() {
        initStats();
        hpCur = hpMax;
        mpCur = mpMax;
        learningPoints = 5;
        skillPoints = 0;
    }

    private void initStats() {
        heroStats = heroClass.stats;
        calcStats();
    }

    @Override
    protected void death() {
        Alert a = new Alert(Alert.AlertType.INFORMATION);
        a.setTitle("GAME OVER");
        a.setHeaderText(name + " is died in the battle.");
        a.setGraphic(null);
        a.showAndWait();
        System.exit(0);
    }

    public void spendStatPointsFX(String stat) {
        switch (stat) {
            case "STR":
                for (Stat x : heroStats) {
                    if (x.type == StatType.STRENGTH) {
                        x.lvlUpValue++;
                    }
                }
                break;
            case "AGI":
                for (Stat x : heroStats) {
                    if (x.type == StatType.AGILITY) {
                        x.lvlUpValue++;
                    }
                }
                break;
            case "VIT":
                for (Stat x : heroStats) {
                    if (x.type == StatType.VITALITY) {
                        x.lvlUpValue++;
                    }
                }
                break;
            case "INT":
                for (Stat x : heroStats) {
                    if (x.type == StatType.INTELLECT) {
                        x.lvlUpValue++;
                    }
                }
                break;
        }
        learningPoints--;
        calcStats(); //todo:recalculate this stat instead of ALL stats
    }

    private void spendSkillPoints() { //todo:TBD
    }

    void expUp(int value) {
        expCur += value;
        lvlUp();
    }

    private void lvlUp() {
        if (expCur >= expMax) {
            level++;
            expCur -= expMax;
            expMax += 100 + expMax * 0.1;
            learningPoints += 3;
            skillPoints += 1;
            Helper.buffer = String.format("%s\nCongratulations! %s reached level %s.\nNow you have free %s skill and %s learning points.\n", Helper.buffer, name, level, skillPoints, learningPoints);
        }
    }

    private void addToInventory(Item i) {
        if (i.isStackable && inventory.contains(i)) {
            Consumable existItem = (Consumable) inventory.get(inventory.indexOf(i));
            existItem.stackCounter++;
            existItem.cost = existItem.baseCost * existItem.stackCounter;
        } else inventory.add(i);
    }

    void drop(Character target) {
        String i0 = "", i1 = "", i2 = "", i3 = "";
        gold += target.gold;
//        drop resources 1-2 todo:TBD
        int r = Helper.getRandom(0, 999);
        if (r < 600) r = 1;
        else if (r < 850) r = 2;
        else if (r < 950) r = 3;
        else r = 4;

        switch (r) {
            case 1: // 60% - consumable;
                Item newItem = Hero.generateRandomConsumable();
                addToInventory(newItem);
                i0 = ", " + newItem.name;
                break;
            case 2: // 25% - item;
                addToInventory(generateRandomEquipableItem());
                i1 = ", " + inventory.get(inventory.size() - 1).name;
                break;
            case 3: // 10% - item + item; // todo: item + consumable
                addToInventory(generateRandomEquipableItem());
                i1 = ", " + inventory.get(inventory.size() - 1).name;
                addToInventory(generateRandomEquipableItem());
                i2 = ", " + inventory.get(inventory.size() - 1).name;
                break;
            case 4: // 5% - item + item + item; // todo: item + item
                addToInventory(generateRandomEquipableItem());
                i1 = ", " + inventory.get(inventory.size() - 1).name;
                addToInventory(generateRandomEquipableItem());
                i2 = ", " + inventory.get(inventory.size() - 1).name;
                addToInventory(generateRandomEquipableItem());
                i3 = ", " + inventory.get(inventory.size() - 1).name;
                break;
        }
        Helper.buffer = String.format("%s\nEarned: %s exp, %s gold%s%s%s.\n", Helper.buffer, target.expCur, target.gold, i0, i1, i2, i3); //win info
    } //move to Enemy class?

    public void inventorySort(Collection<Item> collection) {
        ArrayList<Item> epic = new ArrayList();
        ArrayList<Item> rare = new ArrayList();
        ArrayList<Item> magic = new ArrayList();
        ArrayList<Item> other = new ArrayList();
        for (Item i : collection) {
            switch (i.getItemQuality()) {
                case EPIC:
                    epic.add(i);
                    break;
                case RARE:
                    rare.add(i);
                    break;
                case MAGIC:
                    magic.add(i);
                    break;
                default:
                    other.add(i);
                    break;
            }
        }
        collection.clear();
        collection.addAll(epic);
        collection.addAll(rare);
        collection.addAll(magic);
        collection.addAll(other);
    }

    public static EquipableItem generateRandomEquipableItem() { //todo: move to Item
        EquipableItem newItem = null;
        switch (Helper.getRandom(1, 2)) {
            case 1:
                newItem = new Sword();
                break;
            case 2:
                newItem = new Chest();
                break;
            default:
                System.out.println("\n\ngenerateRandomItem()\n\n");
        }
        return newItem;
    }

    private static Consumable generateRandomConsumable() { //todo: move to Item
        Consumable newItem = null;
        switch (Helper.getRandom(1, 1)) {
            case 1:
                newItem = new HealPotion(1);
                break;
            default:
                System.out.println("\n\ngenerateRandomConsumable()\n\n");
        }
        return newItem;
    }

    public void addItems(int count) { //todo:delete after tests
//        inventory.clear();
        for (int i = 0; i < count; i++) addToInventory(generateRandomEquipableItem());
    }

    void calcStats() {
        for (Stat stat : heroStats) {
            stat.calc();
            switch (stat.type) {
                case STRENGTH:
                    strength = stat.modValue;
                    break;
                case AGILITY:
                    agility = stat.modValue;
                    break;
                case VITALITY:
                    vitality = stat.modValue;
                    break;
                case INTELLECT:
                    intellect = stat.modValue;
                    break;
//                case ATTACK:
//                    attack = stat.modValue;
//                    break;
//                case ARMOR:
//                    armor = stat.modValue;
//                    break;
//                case OFFENSIVE:
//                    offensivePower = stat.modValue;
//                    break;
//                case DEFENSIVE:
//                    defensivePower = stat.modValue;
//                    break;
                default:
                    System.out.println("\n\ncalcStats()\n\n");
            }
        }
        attack = ((strength + level) * 2) + getWeaponAttack(); //todo: remake
        armor = (agility + level) * 2; //todo: remake
        offensivePower = (strength + level) * 3; //todo: remake
        defensivePower = (agility + level) * 3; //todo: remake
        hpMax = heroClass.baseHP + (vitality * 10); //todo: remake
        mpMax = intellect * 10; //todo: remake
    }

    private int getWeaponAttack() {
        if (equipped.get(ItemType.WEAPON) != null) return equipped.get(ItemType.WEAPON).itemStats.get(StatType.ATTACK);
        else return 0;
    }

    public void unequipItem(EquipableItem item) { //unequip item to inventory
        for (Map.Entry<StatType, Integer> item1 : equipped.get(item.itemType).itemStats.entrySet()) {
            for (Stat stat : heroStats) {
                if (stat.type.equals(item1.getKey())) {
                    stat.removeMod(equipped.get(item.itemType));
                }
            }
        }
        inventory.add(equipped.get(item.itemType)); //add unequipped item to inventory list
        equipped.remove(item.itemType); //remove unequipped item from equipped list
        calcStats();
    }
}