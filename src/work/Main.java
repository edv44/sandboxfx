package work;

import javafx.application.Application;
import javafx.stage.Stage;
import work.logic.Enemy;
import work.logic.Hero;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
//        new Switcher(primaryStage, "HelloScreen.fxml", "sandboxFX w1");
//        new Switcher(primaryStage, "MapScreen.fxml", "DEBUG W3");
//        new Switcher(primaryStage, "BattleScreen.fxml", "DEBUG W4");
        new Switcher(primaryStage, "CharacterScreen.fxml", "DEBUG W5");
//        new Switcher(primaryStage, "TownScreen.fxml", "DEBUG W6");
//        new Switcher(primaryStage, "ShopScreen.fxml", "DEBUG W7");
    }

    public static void main(String[] args) {
        launch(args);
    }
}

/*
public class Main {
    static String characterName;
    static int characterClass;

    public static void main(String[] args) {
        gameLoop();
    }

    private static void gameLoop() {
        Helper.clearScreen();
        Hero.getInstance().showInfo();
        System.out.println("\n\nSelect an action: \n1 Character info.\n2 Search for the enemy.\n3 Inventory.\n4 Go to the town.\n5 Use healing potion [#count#].\n\n");
        switch (Helper.read()) {
            case "1": //1 Character info.
                Hero.getInstance().showFullInfo();
                gameLoop();
            case "2": //2 Search for the enemy. todo: Move to another location (bring the mechanism).
                //todo: hero.location > generate monster from this location
                Hero.getInstance().battle(Enemy.generate());
                gameLoop();
            case "3": //3 Inventory.
                Hero.getInstance().getInventoryInfo();
                gameLoop();
            case "4": //4 Go to the town (rest, shop, blacksmith, etc).
                Hero.getInstance().goTown();
                gameLoop();
            case "5": //5 Use healing potion [#count#].
                //todo: use heal potion
                gameLoop();
//            case "95": //95 test adding stats on equip/unequip
//                Hero.getInstance().someMethod();
//            case "96": //96 test adding stats on equip/unequip
//                Hero.getInstance().add10Items();
//                System.out.println("\nAdded 10 items to inventory.");
//            case "97": //97 for item equip/unequip debug
//                for (int item = 0; item <= 15; item++) Hero.getInstance().inventory.add(Hero.generateRandomEquipableItem());
//                Hero.getInstance().getInventoryInfo();
//            case "98": //98 test equip > hashMap
//                for (int item = 0; item <= 150; item++) Hero.getInstance().equipItem(Hero.generateRandomEquipableItem());
//            case "99": //99 test Gear class
//                for (int item = 0; item <= 150; item++) Hero.generateRandomEquipableItem();
            default:
                System.out.println("Please type valid number.");
                gameLoop();
        }
    }
}
*/